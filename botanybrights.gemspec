# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "botanybrights"
  spec.version       = "0.1.0"
  spec.authors       = ["Becca Williams"]
  spec.email         = ["becca@essentialistdev.com"]

  spec.summary       = "Art blog and portfolio theme."
  spec.homepage      = "https://gitlab.com/EssentialistDev/botany-brights"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.1"
end
